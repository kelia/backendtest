package com.kyrielia.navigator.controller

import com.kyrielia.navigator.domain.RouteDetails
import com.kyrielia.navigator.repository.DefaultRouteRepository
import com.kyrielia.navigator.service.ShortestDistanceService
import com.kyrielia.navigator.util.TokenGenerator
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.ValueOperations
import spock.lang.Specification

import java.util.concurrent.ExecutorService

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class TestSubmitLocations extends Specification {

	def namespace = RandomStringUtils.random(10)
	def ttl = new Random().nextInt()

	def tokenGenerator = Mock(TokenGenerator)
	def routeTemplate = Mock(RedisTemplate)
	def executorService = Mock(ExecutorService)
	def valueOperations = Mock(ValueOperations)
	def shortestDistanceService = Mock(ShortestDistanceService)

	def routeRepository = new DefaultRouteRepository(
		navigatorNamespace: namespace,
		routesTtl: ttl,
		tokenGenerator: tokenGenerator,
		routeTemplate: routeTemplate,
	)

	def routeController = new RouteController(
		routeRepository: routeRepository,
		executorService: executorService,
		shortestDistanceService: shortestDistanceService
	)

	def mockMvc = standaloneSetup(routeController).build()

	void 'should return an OK response for a valid list of coordinates'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		1 * tokenGenerator.generateToken() >> RandomStringUtils.random(10)
		1 * routeTemplate.opsForValue() >> valueOperations
		response.andExpect(status().isOk())
	}

	void 'should return token generated for Redis store for a valid list of coordinates'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "${randomLongitude()}"]
		]"""

		def expectedToken = UUID.randomUUID().toString()
		def expectedResponse = "{\"token\": \"${expectedToken}\"}"

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		1 * tokenGenerator.generateToken() >> expectedToken
		1 * routeTemplate.opsForValue() >> valueOperations
		response.andExpect(content().json(expectedResponse))
	}

	void 'should create route details key/value in Redis for a valid list of coordinates'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "${randomLongitude()}"]
		]"""

		def token = UUID.randomUUID().toString()

		def routeDetails = new RouteDetails(
			searchComplete: false,
			error: false
		)

		when:
		mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		1 * tokenGenerator.generateToken() >> token
		1 * valueOperations.set(namespace + token, routeDetails)
		1 * routeTemplate.opsForValue() >> valueOperations
	}

	void 'should submit job to calculate shortest route to executor for a valid list of coordinates'() {
		given:
		def token = UUID.randomUUID().toString()

		def latitude1 = randomLatitude()
		def longitude1 = randomLongitude()
		def latitude2 = randomLatitude()
		def longitude2 = randomLongitude()

		def inputBody = """[
			["${latitude1}", "${longitude1}"],
			["${latitude2}", "${longitude2}"]
		]"""

		when:
		mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		1 * tokenGenerator.generateToken() >> token
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * executorService.submit(_)
	}

	void 'should return bad request for empty list of coordinates'() {
		given:
		def inputBody = "[]"

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should return bad request if only one lat/lon pair given'() {
		given:
		def inputBody = "[[\"${randomLatitude()}\", \"${randomLongitude()}\"]]"

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should return bad request if a lat/lon pair with only 1 value exists in given coordinates'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${new Random().nextDouble()}"],
			["${randomLatitude()}", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should return bad request if a lat/lon pair with more than 2 values exists in given coordinates'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "${randomLongitude()}", "${new Random().nextDouble()}"],
			["${randomLatitude()}", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should return bad request if an empty lat/lon pair exists in given coordinates'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			[],
			["${randomLatitude()}", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should not accept a latitude < -90'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["-90.1", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should not accept a latitude > 90'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["90.1", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should not accept a latitude that is not a number'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${RandomStringUtils.randomAlphabetic(10)}", "${randomLongitude()}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should not accept a longitude < -180'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "-180.1"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should not accept a longitude > 180'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "180.1"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	void 'should not accept a longitude that is not a number'() {
		given:
		def inputBody = """[
			["${randomLatitude()}", "${randomLongitude()}"],
			["${randomLatitude()}", "${RandomStringUtils.randomAlphabetic(10)}"]
		]"""

		when:
		def response = mockMvc.perform(post("/route").content(inputBody).contentType("application/json"))

		then:
		response.andExpect(status().isBadRequest())
	}

	// TODO
	// Repeat pair of coordinates = OK, but remove repeat
	// Test first location is treated as start location

	Double randomLatitude() {
		-90d + new Random().nextDouble() * 180d
	}

	Double randomLongitude() {
		-180d + new Random().nextDouble() * 360d
	}
}
