package com.kyrielia.navigator.controller

import com.kyrielia.navigator.domain.Coordinates
import com.kyrielia.navigator.domain.RouteDetails
import com.kyrielia.navigator.repository.DefaultRouteRepository
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.ValueOperations
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class TestGetRoute extends Specification {

	def namespace = RandomStringUtils.random(10)

	def routeTemplate = Mock(RedisTemplate)
	def valueOperations = Mock(ValueOperations)

	def routeRepository = new DefaultRouteRepository(routeTemplate: routeTemplate, navigatorNamespace: namespace)
	def routeController = new RouteController(routeRepository: routeRepository)

	def mockMvc = standaloneSetup(routeController).build()

	void 'should return 200 OK response if route for token is found in Redis'() {
		given:
		def token = UUID.randomUUID().toString()
		def routeDetails = new RouteDetails()

		when:
		def response = mockMvc.perform(get("/route/$token"))

		then:
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.get(namespace + token) >> routeDetails
		response.andExpect(status().isOk())
	}

	void 'should return 404 not found if route for token is not found in Redis'() {
		given:
		def token = UUID.randomUUID().toString()

		when:
		def response = mockMvc.perform(get("/route/$token"))

		then:
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.get(namespace + token) >> null
		response.andExpect(status().isNotFound())
	}

	void 'should return success response body if route has been calculated'() {
		given:
		def token = UUID.randomUUID().toString()

		def latitude1 = new Random().nextDouble()
		def longitude1 = new Random().nextDouble()
		def latitude2 = new Random().nextDouble()
		def longitude2 = new Random().nextDouble()

		def distance = new Random().nextInt()
		def time = new Random().nextInt()

		ArrayList<ArrayList<Double>> coordinates = [
			[latitude1, longitude1],
			[latitude2, longitude2]
		]

		def routeDetails = new RouteDetails(
			searchComplete: true,
			error: false,
			path: new Coordinates(coordinates),
			distance: distance,
			time: time
		)

		when:
		def response = mockMvc.perform(get("/route/$token"))

		then:
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.get(namespace + token) >> routeDetails

		response.andExpect(content().json("""{
			"status": "success",
			"path": [
				["$latitude1", "$longitude1"],
				["$latitude2", "$longitude2"]
			],
			"total_distance": $distance,
			"total_time": $time
		}"""))
	}

	void 'should return in progress response body if route is still being calculated'() {
		given:
		def token = UUID.randomUUID().toString()

		def routeDetails = new RouteDetails(
			searchComplete: false
		)

		when:
		def response = mockMvc.perform(get("/route/$token"))

		then:
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.get(namespace + token) >> routeDetails

		response.andExpect(content().json("""{
			"status": "in progress"
		}"""))
	}

	void 'should return in error response body if route had an error whilst calculating'() {
		given:
		def token = UUID.randomUUID().toString()
		def errorDescription = RandomStringUtils.random(10)

		def routeDetails = new RouteDetails(
			searchComplete: true,
			error: true,
			errorDescription: errorDescription
		)

		when:
		def response = mockMvc.perform(get("/route/$token"))

		then:
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.get(namespace + token) >> routeDetails

		response.andExpect(content().json("""{
			"status": "failure",
			"error": "$errorDescription"
		}"""))
	}
}
