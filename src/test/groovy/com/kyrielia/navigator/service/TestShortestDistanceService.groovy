package com.kyrielia.navigator.service

import com.kyrielia.navigator.domain.Coordinates
import com.kyrielia.navigator.domain.RouteDetails
import com.kyrielia.navigator.repository.DefaultRouteRepository
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.ValueOperations
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static java.util.concurrent.TimeUnit.HOURS
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

class TestShortestDistanceService extends Specification {

	def googleUrl = 'https://' + RandomStringUtils.random(10, true, false)
	def apiKey = RandomStringUtils.random(10, true, true)

	def namespace = RandomStringUtils.random(10, true, true)
	def ttl = new Random().nextLong()

	def restTemplate = new RestTemplate()
	def routeTemplate = Mock(RedisTemplate)
	def valueOperations = Mock(ValueOperations)

	def googleMapsService = new GoogleMapsService(
		matrixApiUrl: googleUrl,
		apiKey: apiKey,
		googleRestTemplate: restTemplate
	)

	def routeRepository = new DefaultRouteRepository(
		navigatorNamespace: namespace,
		routesTtl: ttl,
		routeTemplate: routeTemplate
	)

	def service = new ShortestDistanceService(
		googleMapsService: googleMapsService,
		routeRepository: routeRepository
	)

	def setup() {
		googleMapsService.initialise()
	}

	void 'should find shortest path ABC and save results to Redis'() {
		given:
		def token = UUID.randomUUID().toString()

		def latitude1 = 22.372081
		def longitude1 = 114.107877
		def latitude2 = 22.326442
		def longitude2 = 114.167811
		def latitude3 = 22.284419
		def longitude3 = 114.15951

		def coordinates = new Coordinates([
			[latitude1, longitude1],
			[latitude2, longitude2],
			[latitude3, longitude3]
		])

		def expectedPath = new Coordinates([
			[latitude1, longitude1],
			[latitude2, longitude2],
			[latitude3, longitude3]
		])

		def mockService = MockRestServiceServer.createServer(restTemplate)

		def url = "$googleUrl?origins=$latitude1,$longitude1%7C$latitude2,$longitude2%7C$latitude3,$longitude3&destinations=$latitude1,$longitude1%7C$latitude2,$longitude2%7C$latitude3,$longitude3&units=metric&key=$apiKey"
		mockService.expect(requestTo(url))
			.andRespond(withSuccess("""{
					"status": "OK",
					"rows": [
						{
							"elements": [
								{
									"duration": { "value": 0 },
									"distance": { "value": 0 }
								},
								{
									"duration": { "value": 1 },
									"distance": { "value": 1 }
								},
								{
									"duration": { "value": 1 },
									"distance": { "value": 2 }
								}
							]
						},
						{
							"elements": [
								{
									"duration": { "value": 2 },
									"distance": { "value": 3 }
								},
								{
									"duration": { "value": 0 },
									"distance": { "value": 0 }
								},
								{
									"duration": { "value": 1 },
									"distance": { "value": 4 }
								}
							]
						},
						{
							"elements": [
								{
									"duration": { "value": 3 },
									"distance": { "value": 5 }
								},
								{
									"duration": { "value": 3 },
									"distance": { "value": 6 }
								},
								{
									"duration": { "value": 0 },
									"distance": { "value": 0 }
								}
							]
						}
					]
				}""", APPLICATION_JSON))

		when:
		service.calculateShortestDistance(token, coordinates)

		then:
		mockService.verify()

		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.set(namespace + token, _) >> { params ->
			def routeDetails = params[1] as RouteDetails
			assert routeDetails.searchComplete
			assert !routeDetails.error
			assert routeDetails.distance == 5
			assert routeDetails.time == 2
			assert routeDetails.path.coordinates == expectedPath.coordinates
		}
		1 * routeTemplate.expire(namespace + token, ttl, HOURS)
	}

	void 'should find shortest path ACB and save results to Redis'() {
		given:
		def token = UUID.randomUUID().toString()

		def latitude1 = 22.372081
		def longitude1 = 114.107877
		def latitude2 = 22.326442
		def longitude2 = 114.167811
		def latitude3 = 22.284419
		def longitude3 = 114.15951

		def coordinates = new Coordinates([
			[latitude1, longitude1],
			[latitude2, longitude2],
			[latitude3, longitude3]
		])

		def expectedPath = new Coordinates([
			[latitude1, longitude1],
			[latitude3, longitude3],
			[latitude2, longitude2]
		])

		def mockService = MockRestServiceServer.createServer(restTemplate)

		def url = "$googleUrl?origins=$latitude1,$longitude1%7C$latitude2,$longitude2%7C$latitude3,$longitude3&destinations=$latitude1,$longitude1%7C$latitude2,$longitude2%7C$latitude3,$longitude3&units=metric&key=$apiKey"
		mockService.expect(requestTo(url))
			.andRespond(withSuccess("""{
					"status": "OK",
					"rows": [
						{
							"elements": [
								{
									"duration": { "value": 0 },
									"distance": { "value": 0 }
								},
								{
									"duration": { "value": 3 },
									"distance": { "value": 6 }
								},
								{
									"duration": { "value": 4 },
									"distance": { "value": 5 }
								}
							]
						},
						{
							"elements": [
								{
									"duration": { "value": 3 },
									"distance": { "value": 4 }
								},
								{
									"duration": { "value": 0 },
									"distance": { "value": 0 }
								},
								{
									"duration": { "value": 2 },
									"distance": { "value": 3 }
								}
							]
						},
						{
							"elements": [
								{
									"duration": { "value": 2 },
									"distance": { "value": 2 }
								},
								{
									"duration": { "value": 1 },
									"distance": { "value": 1 }
								},
								{
									"duration": { "value": 0 },
									"distance": { "value": 0 }
								}
							]
						}
					]
				}""", APPLICATION_JSON))

		when:
		service.calculateShortestDistance(token, coordinates)

		then:
		mockService.verify()

		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.set(namespace + token, _) >> { params ->
			def routeDetails = params[1] as RouteDetails
			assert routeDetails.searchComplete
			assert !routeDetails.error
			assert routeDetails.distance == 6
			assert routeDetails.time == 5
			assert routeDetails.path.coordinates == expectedPath.coordinates
		}
		1 * routeTemplate.expire(namespace + token, ttl, HOURS)
	}

	void 'should update Redis with error status and message if Google API returns error response'() {
		given:
		def token = UUID.randomUUID().toString()

		def latitude1 = randomLatitude()
		def longitude1 = randomLongitude()
		def latitude2 = randomLatitude()
		def longitude2 = randomLongitude()

		def coordinates = new Coordinates([
			[latitude1, longitude1],
			[latitude2, longitude2]
		])

		def mockService = MockRestServiceServer.createServer(restTemplate)

		def url = "$googleUrl?origins=$latitude1,$longitude1%7C$latitude2,$longitude2&destinations=$latitude1,$longitude1%7C$latitude2,$longitude2&units=metric&key=$apiKey"
		mockService.expect(requestTo(url))
			.andRespond(withSuccess("""{
					"status": "$a"
				}""", APPLICATION_JSON))

		when:
		service.calculateShortestDistance(token, coordinates)

		then:
		mockService.verify()
		1 * routeTemplate.opsForValue() >> valueOperations
		1 * valueOperations.set(namespace + token, _) >> { params ->
			def routeDetails = params[1] as RouteDetails
			assert routeDetails.searchComplete
			assert routeDetails.error
		}
		1 * routeTemplate.expire(namespace + token, ttl, HOURS)

		where:
		a                           | _
		'INVALID_REQUEST'           | _
		'OVER_QUERY_LIMIT'          | _
		'REQUEST_DENIED'            | _
		'UNKNOWN_ERROR'             | _
		'MAX_ELEMENTS_EXCEEDED'     | _
		'NOT_FOUND'                 | _
		'ZERO_RESULTS'              | _
		'MAX_ROUTE_LENGTH_EXCEEDED' | _
	}

	Double randomLatitude() {
		-90d + new Random().nextDouble() * 180d
	}

	Double randomLongitude() {
		-180d + new Random().nextDouble() * 360d
	}
}
