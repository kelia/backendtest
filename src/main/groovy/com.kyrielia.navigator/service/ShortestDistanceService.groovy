package com.kyrielia.navigator.service

import com.kyrielia.navigator.domain.Coordinate
import com.kyrielia.navigator.domain.Coordinates
import com.kyrielia.navigator.domain.RouteDetails
import com.kyrielia.navigator.domain.google.Element
import com.kyrielia.navigator.exception.GoogleMatrixApiException
import com.kyrielia.navigator.repository.RouteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * A service to calculate the shortest distance for a list of coordinates. Results for the calculation are saved
 * to Redis
 */
@Service
class ShortestDistanceService {

	@Autowired
	private GoogleMapsService googleMapsService

	@Autowired
	private RouteRepository routeRepository

	void calculateShortestDistance(String token, Coordinates coordinates) {

		routeRepository.update(token, getRouteDetails(coordinates))
	}

	private RouteDetails getRouteDetails(Coordinates coordinates) {

		int shortestDistance = Integer.MAX_VALUE
		int timeForDistance = Integer.MAX_VALUE
		ArrayList<Coordinate> shortestRoute = []

		def distanceMatrix
		try {
			distanceMatrix = googleMapsService.getDistanceMatrix(coordinates)
		} catch (GoogleMatrixApiException e) {
			// If error response found in call to Google Maps, save error details to Redis
			return new RouteDetails(
				searchComplete: true,
				error: true,
				errorDescription: e.message
			)
		}

		// find all possible routes from starting coordinate to the rest of the coordinates
		// Using an array of integers to make it easier to query the distance matrix
		ArrayList<ArrayList<Integer>> routes = generatePossibleRoutes(coordinates)

		// Check all possible routes against the distance matrix to find the shortest route
		routes.each { route ->
			int totalDistance = 0
			int totalTime = 0

			for (def i = 0; i < route.size() - 1; i++) {
				def currentCoordinate = route.get(i)
				def nextCoordinate = route.get(i + 1)

				Element element = distanceMatrix.get(currentCoordinate)
					.elements.get(nextCoordinate)

				totalDistance += element.distance.value
				totalTime += element.duration.value
			}

			if (totalDistance < shortestDistance) {
				shortestDistance = totalDistance
				timeForDistance = totalTime
				shortestRoute.clear()

				for (def index : route) {
					shortestRoute.add(coordinates.coordinates.get(index))
				}
			}
		}

		def routeCoordinates = new Coordinates()
		routeCoordinates.coordinates = shortestRoute

		new RouteDetails(
			searchComplete: true,
			error: false,
			path: routeCoordinates,
			distance: shortestDistance,
			time: timeForDistance
		)
	}

	private ArrayList<ArrayList<Integer>> generatePossibleRoutes(Coordinates coordinates) {

		ArrayList<Integer> destinationIndexes = []
		(1..(coordinates.coordinates.size() - 1)).each {
			destinationIndexes += it
		}

		findPossibleRoutes(0, destinationIndexes)
	}

	private ArrayList<ArrayList<Integer>> findPossibleRoutes(Integer startNode, ArrayList<Integer> destinationNodes) {

		ArrayList<ArrayList<Integer>> routes = []

		if (destinationNodes.size() == 2) {
			// Only two possible combinations with 2 nodes: A -> B and B -> A
			def route1 = [startNode]
			route1.add(destinationNodes[0])
			route1.add(destinationNodes[1])

			def route2 = [startNode]
			route2.add(destinationNodes[1])
			route2.add(destinationNodes[0])

			routes.add(route1)
			routes.add(route2)
			return routes
		}

		// If > 2 destinations, use a recursive function to get all routes for the destinations
		for (def i = 0; i < destinationNodes.size(); i++) {
			def headNode = destinationNodes[i]
			def tailNodes = destinationNodes.clone() as ArrayList<Integer>
			tailNodes.remove(i)
			def results = findPossibleRoutes(headNode, tailNodes)

			results.each { result ->
				def route = [startNode]
				route.addAll(result)
				routes.add(route)
			}
		}

		routes
	}
}
