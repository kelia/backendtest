package com.kyrielia.navigator.service

import com.kyrielia.navigator.domain.Coordinates
import com.kyrielia.navigator.domain.google.GoogleDistanceMatrixApiResponse
import com.kyrielia.navigator.domain.google.Row
import com.kyrielia.navigator.exception.GoogleMatrixApiException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponents

import javax.annotation.PostConstruct

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl

/**
 * A service to handle API calls to Google Maps APIs
 */
@Service
class GoogleMapsService {

	@Value('${google.maps.matrix.url}')
	private String matrixApiUrl

	@Value('${google.maps.matrix.apikey}')
	private String apiKey

	@Autowired
	private RestTemplate googleRestTemplate

	private UriComponents googleDistanceMatrixComponents

	@PostConstruct
	public void initialise() {

		googleDistanceMatrixComponents = fromHttpUrl(matrixApiUrl)
			.queryParam('origins', '{origins}')
			.queryParam('destinations', '{destinations}')
			.queryParam('units', 'metric')
			.queryParam('key', apiKey)
			.build()
	}

	List<Row> getDistanceMatrix(Coordinates coordinates) {

		def coordinateString = coordinates.toPipeSeparatedList()
		def uri = googleDistanceMatrixComponents.expand(coordinateString, coordinateString).toUri()

		def response
		try {
			response = googleRestTemplate.getForObject(uri, GoogleDistanceMatrixApiResponse)
		} catch (RestClientException e) {
			println(e.getMessage())
			throw e
		}

		if (response.status != 'OK') {
			handleErrorResponse(response)
		}

		response.rows
	}

	void handleErrorResponse(GoogleDistanceMatrixApiResponse errorResponse) {

		switch (errorResponse.status) {
			case 'INVALID_REQUEST':
			case 'OVER_QUERY_LIMIT':
			case 'REQUEST_DENIED':
			case 'UNKNOWN_ERROR':
				throw new GoogleMatrixApiException('Internal error occurred when processing route')

			case 'MAX_ELEMENTS_EXCEEDED':
				throw new GoogleMatrixApiException('Could not process route due to too many destinations')

			case 'NOT_FOUND':
				throw new GoogleMatrixApiException('Could not process route as 1 or more coordinates could not be geocoded')

			case 'ZERO_RESULTS':
				throw new GoogleMatrixApiException('Could not find an available route')

			case 'MAX_ROUTE_LENGTH_EXCEEDED':
				throw new GoogleMatrixApiException('Could not process route as the requested route is too long')
		}
	}
}
