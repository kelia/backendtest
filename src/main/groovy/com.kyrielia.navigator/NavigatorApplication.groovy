package com.kyrielia.navigator

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * Access point to the application
 */
@SpringBootApplication
class NavigatorApplication {

	public static void main(String[] args) {

		SpringApplication.run(NavigatorApplication, args)
	}
}
