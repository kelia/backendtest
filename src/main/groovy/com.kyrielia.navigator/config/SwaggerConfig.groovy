package com.kyrielia.navigator.config

import com.google.common.base.Predicate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

import java.time.ZonedDateTime

import static springfox.documentation.builders.PathSelectors.regex

@Configuration
@EnableSwagger2
class SwaggerConfig {

	@Bean
	Docket customImplementation() {

		new Docket(DocumentationType.SWAGGER_2)
			.apiInfo(apiInfo())
			.useDefaultResponseMessages(false)
			.directModelSubstitute(ZonedDateTime, Date)
			.select()
			.paths(paths())
			.build();
	}

	private static ApiInfo apiInfo() {

		new ApiInfo("Navigator",
			"A service that calculates the shortest path between coordinates",
			"1.0",
			null,
			null,
			null,
			null)
	}

	private static Predicate<String> paths() {

		regex("/route.*");
	}
}
