package com.kyrielia.navigator.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Configuration for the app
 */
@Configuration
class AppConfig {

	@Value('${navigator.threads.max}')
	private int threadPoolSize = 1

	@Bean
	public RestTemplate googleRestTemplate() {
		new RestTemplate();
	}

	@Bean
	public ExecutorService executorService() {
		Executors.newFixedThreadPool(threadPoolSize)
	}
}
