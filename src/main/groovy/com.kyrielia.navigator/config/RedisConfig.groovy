package com.kyrielia.navigator.config

import com.kyrielia.navigator.domain.RouteDetails
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.DependsOn
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.session.data.redis.config.ConfigureRedisAction
import redis.clients.jedis.JedisPoolConfig

/**
 * Configuration for the app's connection settings to Redis
 */
@Configuration
class RedisConfig {

	@Value('${spring.redis.host}')
	private String redisHost

	@Value('${spring.redis.port}')
	private Integer redisPort

	@Value('${spring.redis.pool.maxTotal}')
	private Integer maxTotal

	@Bean
	public static ConfigureRedisAction configureRedisAction() {

		ConfigureRedisAction.NO_OP
	}

	@Bean
	public JedisPoolConfig jedisPoolConfig() {

		def jedisPoolConfig = new JedisPoolConfig()
		jedisPoolConfig.setMaxTotal(maxTotal)
		jedisPoolConfig
	}

	@Bean
	@DependsOn("jedisPoolConfig")
	public JedisConnectionFactory connectionFactory() {

		def jedisConnectionFactory = new JedisConnectionFactory()
		jedisConnectionFactory.setUsePool(true)
		jedisConnectionFactory.setHostName(redisHost)
		jedisConnectionFactory.setPort(redisPort)
		jedisConnectionFactory.setPoolConfig(jedisPoolConfig())
		jedisConnectionFactory
	}

	@Bean
	@DependsOn("connectionFactory")
	public RedisTemplate routeTemplate() {

		RedisTemplate<String, RouteDetails> routeRedisTemplate = new RedisTemplate<>();
		routeRedisTemplate.setConnectionFactory(connectionFactory());
		routeRedisTemplate;
	}
}
