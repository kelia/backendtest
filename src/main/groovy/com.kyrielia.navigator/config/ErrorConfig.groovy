package com.kyrielia.navigator.config

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes
import org.springframework.boot.autoconfigure.web.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.context.request.RequestAttributes

import javax.annotation.PostConstruct
import java.text.DateFormat
import java.text.SimpleDateFormat

import static java.util.TimeZone.getTimeZone

/**
 * Removes default Spring exception field from API responses.
 */
@Configuration
public class ErrorConfig {

	private static final String EXCEPTION = 'exception'

	private static final String TIMESTAMP = 'timestamp'

	private DateFormat iso8601DateFormat

	@PostConstruct
	public void initialise() {
		iso8601DateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
		iso8601DateFormat.setTimeZone(getTimeZone('UTC'))
	}

	@Bean
	public ErrorAttributes errorAttributes() {

		new DefaultErrorAttributes() {

			@Override
			public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {

				Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace)

				if (errorAttributes.containsKey(TIMESTAMP)) {
					Date date = (Date) errorAttributes.get(TIMESTAMP)
					errorAttributes.put(TIMESTAMP, iso8601DateFormat.format(date))
				}

				if (errorAttributes.containsKey(EXCEPTION)) {
					errorAttributes.remove(EXCEPTION)
				}

				errorAttributes
			}
		}
	}
}
