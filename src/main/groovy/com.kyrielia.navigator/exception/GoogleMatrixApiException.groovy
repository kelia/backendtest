package com.kyrielia.navigator.exception

/**
 * An exception to be thrown if a non-OK status is returned from the Google Matrix API
 */
class GoogleMatrixApiException extends RuntimeException {

	GoogleMatrixApiException(String message) {
		super(message)
	}
}
