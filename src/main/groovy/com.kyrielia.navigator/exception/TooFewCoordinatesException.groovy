package com.kyrielia.navigator.exception

import org.springframework.web.bind.annotation.ResponseStatus

import static org.springframework.http.HttpStatus.BAD_REQUEST

/**
 * An exception to be thrown when only 0 or 1 coordinate pairs are submitted by an API user
 */
@ResponseStatus(BAD_REQUEST)
class TooFewCoordinatesException extends RuntimeException {

	TooFewCoordinatesException(String message) {
		super(message)
	}
}
