package com.kyrielia.navigator.exception

import org.springframework.web.bind.annotation.ResponseStatus

import static org.springframework.http.HttpStatus.NOT_FOUND

/**
 * An exception to be thrown if a route token cannot be found
 */
@ResponseStatus(NOT_FOUND)
class RouteNotFoundException extends RuntimeException {

	RouteNotFoundException(String message) {
		super(message)
	}
}
