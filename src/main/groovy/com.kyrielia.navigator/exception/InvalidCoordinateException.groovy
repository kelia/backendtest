package com.kyrielia.navigator.exception

import org.springframework.web.bind.annotation.ResponseStatus

import static org.springframework.http.HttpStatus.BAD_REQUEST

/**
 * An exception to be thrown when an invalid coordinate is submitted by an API user
 */
@ResponseStatus(BAD_REQUEST)
class InvalidCoordinateException extends RuntimeException {

	InvalidCoordinateException(String message) {
		super(message)
	}
}
