package com.kyrielia.navigator.controller

import com.kyrielia.navigator.domain.Coordinates
import com.kyrielia.navigator.domain.RouteDetails
import com.kyrielia.navigator.domain.response.RouteResponse
import com.kyrielia.navigator.domain.response.TokenResponse
import com.kyrielia.navigator.exception.RouteNotFoundException
import com.kyrielia.navigator.repository.RouteRepository
import com.kyrielia.navigator.service.ShortestDistanceService
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import java.util.concurrent.ExecutorService

import static org.apache.http.HttpStatus.*
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import static org.springframework.http.ResponseEntity.ok
import static org.springframework.web.bind.annotation.RequestMethod.GET
import static org.springframework.web.bind.annotation.RequestMethod.POST

/**
 * A controller to handle API operations for submitting route locations to work out the shortest driving distance, and
 * to retrieve the results of this operation.
 */
@RestController
@RequestMapping('/route')
class RouteController {

	@Autowired
	private ExecutorService executorService

	@Autowired
	private RouteRepository routeRepository

	@Autowired
	private ShortestDistanceService shortestDistanceService

	@ApiOperation(value = 'Submit a list of coordinates in order to calculate the shortest path between them')
	@ApiImplicitParam(
		name = 'coordinatesBody',
		value = 'The list of coordinates to submit',
		required = true,
		dataType = 'ArrayList[ArrayList[Double]]',
		paramType = 'body'
	)
	@ApiResponses([
		@ApiResponse(code = SC_OK, message = 'Coordinates have been successfully submitted for processing', response = TokenResponse),
		@ApiResponse(code = SC_BAD_REQUEST, message = 'Invalid coordinates have been submitted')
	])
	@RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	ResponseEntity<TokenResponse> submitLocations(@RequestBody ArrayList<ArrayList<Double>> coordinatesBody) {

		def coordinates = new Coordinates(coordinatesBody)

		def token = routeRepository.create(new RouteDetails())

		// Submit calculation of shortest distance to an executor to allow calculation to be asynchronous
		executorService.submit({ shortestDistanceService.calculateShortestDistance(token, coordinates) })

		ok().body(new TokenResponse(token))
	}

	@ApiOperation(value = 'Retrieves the results of a single shortest path calculation')
	@ApiImplicitParam(
		name = 'token',
		value = 'A token used to retrieve the results of a shortest path calculation',
		paramType = 'path',
		required = true
	)
	@ApiResponses([
		@ApiResponse(code = SC_OK, message = 'Results found for given token'),
		@ApiResponse(code = SC_NOT_FOUND, message = 'No results found for given token')
	])
	@RequestMapping(value = '/{token}', method = GET, produces = APPLICATION_JSON_VALUE)
	ResponseEntity<RouteResponse> getDrivingRouteDetails(@PathVariable String token) {

		def route = routeRepository.getRoute(token)

		if (!route) {
			throw new RouteNotFoundException("No route found for token '$token'")
		}

		def routeResponse

		if (route.error) {
			routeResponse = new RouteResponse(
				status: 'failure',
				error: route.errorDescription,
				distance: null,
				time: null
			)
		} else if (route.searchComplete) {
			 routeResponse = new RouteResponse(
				status: 'success',
				path: route.path.toStringList(),
				distance: route.distance,
				time: route.time
			)
		} else {
			routeResponse = new RouteResponse(status: 'in progress')
		}

		ok().body(routeResponse)
	}
}
