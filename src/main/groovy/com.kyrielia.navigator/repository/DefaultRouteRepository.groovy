package com.kyrielia.navigator.repository

import com.kyrielia.navigator.domain.RouteDetails
import com.kyrielia.navigator.util.TokenGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Component

import static java.util.concurrent.TimeUnit.HOURS

/**
 * Handle CRUD operations for routes in Redis
 */
@Component
class DefaultRouteRepository implements RouteRepository {

	@Value('${navigator.redis.routes.namespace}')
	private String navigatorNamespace;

	@Value('${navigator.redis.routes.ttl}')
	private Long routesTtl;

	@Autowired
	private TokenGenerator tokenGenerator

	@Autowired
	private RedisTemplate<String, RouteDetails> routeTemplate

	@Override
	String create(RouteDetails routeDetails) {

		def token = tokenGenerator.generateToken()
		routeTemplate.opsForValue().set(generateKey(token), routeDetails)
		token
	}

	@Override
	void update(String token, RouteDetails routeDetails) {

		routeTemplate.opsForValue().set(generateKey(token), routeDetails)
		routeTemplate.expire(generateKey(token), routesTtl, HOURS);
	}

	@Override
	RouteDetails getRoute(String token) {
		routeTemplate.opsForValue().get(generateKey(token))
	}

	private String generateKey(String token) {
		navigatorNamespace + token
	}
}
