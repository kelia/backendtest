package com.kyrielia.navigator.repository

import com.kyrielia.navigator.domain.RouteDetails

/**
 * An interface for the repository for route information
 */
interface RouteRepository {

	String create(RouteDetails routeDetails)

	void update(String token, RouteDetails routeDetails)

	RouteDetails getRoute(String token)
}
