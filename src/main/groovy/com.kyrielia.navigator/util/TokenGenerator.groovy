package com.kyrielia.navigator.util

import org.springframework.stereotype.Component

/**
 * A utility class to generate random UUID tokens
 */
@Component
class TokenGenerator {

	@SuppressWarnings("GrMethodMayBeStatic")
	String generateToken() {
		UUID.randomUUID().toString()
	}
}
