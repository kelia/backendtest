package com.kyrielia.navigator.domain

import com.kyrielia.navigator.exception.InvalidCoordinateException

/**
 * An object corresponding to a single lat/lon pair
 */
class Coordinate implements Serializable {

	Double latitude
	Double longitude

	Coordinate(ArrayList<Double> coordinate) {

		if (coordinate.size() != 2) {
			throw new InvalidCoordinateException("Two values per coordinate is required")
		}

		Double latitude = coordinate[0]
		if (latitude < -90.0 || latitude > 90.0) {
			throw new InvalidCoordinateException("Invalid latitude value: $latitude")
		}

		Double longitude = coordinate[1]
		if (longitude < -180.0 || longitude > 180.0) {
			throw new InvalidCoordinateException("Invalid longitude value: $longitude")
		}

		this.latitude = latitude
		this.longitude = longitude
	}

	Double getLatitude() {
		latitude
	}

	Double getLongitude() {
		longitude
	}

	List<String> toStringList() {
		["${latitude}" as String, "${longitude}" as String]
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		Coordinate that = (Coordinate) o

		if (latitude != that.latitude) return false
		if (longitude != that.longitude) return false

		return true
	}

	int hashCode() {
		int result
		result = (latitude != null ? latitude.hashCode() : 0)
		result = 31 * result + (longitude != null ? longitude.hashCode() : 0)
		return result
	}
}
