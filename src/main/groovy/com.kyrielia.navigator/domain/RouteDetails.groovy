package com.kyrielia.navigator.domain

import groovy.transform.Canonical

/**
 * Details for a route. This object is stored in Redis to be retrieved through the get shortest route API.
 */
@Canonical
class RouteDetails implements Serializable {

	boolean searchComplete = false

	boolean error = false

	Coordinates path

	int distance

	int time

	String errorDescription
}
