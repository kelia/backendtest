package com.kyrielia.navigator.domain.google

import groovy.transform.Canonical

/**
 * A POJO to represent the distance in Google matrix API responses
 */
@Canonical
class Distance {

	int value
}
