package com.kyrielia.navigator.domain.google

import groovy.transform.Canonical

/**
 * A POJO to represent a row in Google matrix API responses
 */
@Canonical
class Row {

	List<Element> elements
}
