package com.kyrielia.navigator.domain.google

import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical

/**
 * A POJO to represent responses from the Google matrix API responses
 */
@Canonical
class GoogleDistanceMatrixApiResponse {

	String status

	List<Row> rows

	@JsonProperty('error_message')
	String errorMessage
}
