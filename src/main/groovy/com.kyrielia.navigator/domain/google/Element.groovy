package com.kyrielia.navigator.domain.google

import groovy.transform.Canonical

/**
 * A POJO to represent an element in Google matrix API responses
 */
@Canonical
class Element {

	Duration duration

	Distance distance
}
