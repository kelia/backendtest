package com.kyrielia.navigator.domain.google

import groovy.transform.Canonical

/**
 * A POJO to represent duration in Google matrix API responses
 */
@Canonical
class Duration {

	int value
}
