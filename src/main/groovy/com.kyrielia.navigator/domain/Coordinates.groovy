package com.kyrielia.navigator.domain

import com.kyrielia.navigator.exception.TooFewCoordinatesException

/**
 * An object corresponding to a list of coordinates
 */
class Coordinates implements Serializable {

	private List<Coordinate> coordinates = new ArrayList<>()

	Coordinates() {}

	Coordinates(ArrayList<ArrayList<Double>> coordinates) {

		if (coordinates.size() < 2) {
			throw new TooFewCoordinatesException("Two or more coordinates required")
		}

		for (ArrayList<Double> coordinate : coordinates) {
			this.coordinates.add(new Coordinate(coordinate))
		}
	}

	List<Coordinate> getCoordinates() {
		coordinates
	}

	void setCoordinates(List<Coordinate> coordinates) {
		this.coordinates = coordinates
	}

	List<List<String>> toStringList() {

		List<List<String>> coordinateList = []
		for (Coordinate coordinate : coordinates) {
			coordinateList.add(coordinate.toStringList())
		}

		coordinateList
	}

	String toPipeSeparatedList() {

		def pipeSeparatedList = ""
		for (Coordinate coordinate : coordinates) {
			pipeSeparatedList += "${coordinate.latitude},${coordinate.longitude}|"
		}

		pipeSeparatedList.substring(0, pipeSeparatedList.length() - 1)
	}
}
