package com.kyrielia.navigator.domain.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical

/**
 * Response object for the shortest driving route API
 */
@Canonical
@JsonInclude(JsonInclude.Include.NON_NULL)
class RouteResponse {

	String status

	List<List<String>> path

	@JsonProperty('total_distance')
	Integer distance

	@JsonProperty('total_time')
	Integer time

	String error
}
