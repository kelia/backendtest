package com.kyrielia.navigator.domain.response

/**
 * Response object for the submit locations API
 */
class TokenResponse {

	final String token

	TokenResponse(String token) {
		this.token = token;
	}

	String getToken() {
		token
	}
}
