FROM openjdk:8

RUN wget -q https://services.gradle.org/distributions/gradle-4.0-bin.zip \
    && unzip gradle-4.0-bin.zip -d /opt \
    && rm gradle-4.0-bin.zip

ENV GRADLE_HOME /opt/gradle-4.0
ENV PATH $PATH:/opt/gradle-4.0/bin

ADD . / /src/
WORKDIR /src
EXPOSE 8080
RUN gradle build --stacktrace
ENTRYPOINT ["gradle", "bootRun"]
