# Navigator

An API to calculate and retrieve the shortest distance from a starting point to a list of other destinations. 
All destinations are treated as lat/lon coordinates.

## Prerequisites

* Docker
* Docker Compose

## Design

The app is split into three parts:

* An API to receive a list of coordinates and submit them to an asynchronous job to start the calculation.
* An asynchronous process that calculates the shortest path based on the coordinates.
* An API to retrieve the results of the calculation.

The status and results of the calculation are stored in a Redis cache to allow the application to be scaled, as there
is no guarantee that the POST and GET requests will reach the same instance of the application if there is more than one
instance. Having a Redis cluster also separates the status of the route calculations from the state of the app, i.e. if
an instance of the app goes down, we can still retrieve the document. A TTL is also set on the Redis documents to ensure 
we only hold on to 24 hours of data.

The calculation to work out the shortest route makes use of the Google Distance Matrix API to retrieve a list of all
distances between every coordinate submitted in a route. All possible routes from the start coordinates are calculated 
and compared in order to find the shortest path.

## Running the application

```
docker-compose build
docker-compose up
```

The service has been set up with Swagger documentation to allow integrators to the API to browse to an interactive set
of API docs. You can browse to `http://localhost:8080/swagger-ui.html` to access these documents and interact with the
APIs directly, instead of having to use Curl/Postman/etc.
